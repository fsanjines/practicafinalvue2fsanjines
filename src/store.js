import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        listaNotas: [{
            titulo: 'Nota 1',
            descripcion: 'Descripcion 1'
        }],
        usuarios: [
        {
            usuario: 'admin',
            contrasena: 'admin',
            rol: 'ADMIN'
        },
        {
            usuario: 'tecnico',
            contrasena: 'tecnico',
            rol: 'TECNICO'
        }
        ]
    },
    mutations: {
        adicionarNota (state, nota) {
            state.listaNotas.push(nota)
        },
        eliminarNota (state, index) {
            state.listaNotas.splice(index, 1) 
        }
    }
})