import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const guardApp = (to, from, next) => {
  const _existetoken = window.localStorage.getItem('token')
  if (_existetoken) {
    next()
  }
  else {
    next('/')
  }
}

const guardLogin = (to, from, next) => {
  const _existetoken = window.localStorage.getItem('token')
  if (_existetoken) {
    return next('/app')
  }
  else {
    return next()
  }
}


const routes = [
  {
    path: '/',
    name: 'OutLogin',
    component: () => import('../layouts/OutLoginlayout.vue'),
    beforeEnter: guardLogin,
    children: [
      { 
        path: '',
        name: 'Login',
        component: () => import('../views/Login.vue')
      }
    ]
  },
 
  // LAYOUT ADMINISTRADORES
  {
    path: '/app',
    name: 'App',
    component: () => import('../layouts/MainLayout.vue'),
    beforeEnter: guardApp,
    children: [
      { 
        path: '',
        name: 'Inicio',
        component: () => import('../views/inicio.vue')
      },
      {
        path: 'usuarios',
        name: 'Usuarios',
        component: () => import('../views/Usuarios.vue')
      },
      {
        path: 'roles',
        name: 'Roles',
        component: () => import('../views/Roles.vue')
      }
    ]

  },

  // LAYOUT TECNICOS
  {
    path: '/tech',
    name: 'Technicals',
    component: () => import('../layouts/TecnicoLayout.vue'),
    beforeEnter: guardApp,
    children: [
      { 
        path: '',
        name: 'Inicio',
        component: () => import('../views/inicio.vue')
      },
    ]

  },
  
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
